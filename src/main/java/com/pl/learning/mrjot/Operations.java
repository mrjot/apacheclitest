package com.pl.learning.mrjot;

import java.io.IOException;

public class Operations {

	public static double mult(double a, double b) {
		return a * b;
	}
	
	

	public static double div(double a, double b) {
		if (b == 0) {
			throw new ArithmeticException("Dzielenie przez 0 !");
		}
		return a / b;
	}
	

	public static double sum(double a, double b) {
		return a + b;
	}
	
	

	public static double sub(double a, double b) {
		return a - b;
	}

}
