package com.pl.learning.mrjot;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

public class Cli {

	Options options = new Options();
	
	private String[] args;
	
	public Cli(String[] args) {
		
		this.args = args;
		
		options.addOption("mult","multiply", false, "multiply two numbers");
		options.addOption("sub", "substract", false, "substract two numbers");
		options.addOption("sum", "summ",false, "sum two numbers");
		options.addOption("div", "divide", false, "divide two numbers");
		
	}
	
	public void parse() {
		
		System.out.println("--------------------------------------------------------");
		
		System.out.println("Witaj w aplikacji kalkulator ! Aplikacja nie jest może \n"
				+ "spektakularna. Jej celem było przetestowanie Apache CLI"
				+ "" );
		
		System.out.println("--------------------------------------------------------");
		
		help();
		
		CommandLineParser parser = new DefaultParser();
		try {
			CommandLine cmd = parser.parse( options, args);
			if(cmd.hasOption("mult")) {
				ReadData data = readDataForCalcs();
				data.printResult(Operations.mult(data.getFirstNumber(), data.getSecondNumber()));
			}if(cmd.hasOption("sub")){
				ReadData data = readDataForCalcs();
				data.printResult(Operations.sub(data.getFirstNumber(), data.getSecondNumber()));	
			}if(cmd.hasOption("sum")){
				ReadData data = readDataForCalcs();
				data.printResult(Operations.sum(data.getFirstNumber(), data.getSecondNumber()));
			}if(cmd.hasOption("div")){
				ReadData data = readDataForCalcs();
				data.printResult(Operations.div(data.getFirstNumber(), data.getSecondNumber()));
			}
			
		}catch (ParseException e){
			System.out.println("Niewłaściwy wybór:");
			help();
			
		}

	}

	private ReadData readDataForCalcs() {
		ReadData data = new ReadData();
		data.getDataFromUser();
		return data;
	}

	private void help() {
		
		  HelpFormatter formater = new HelpFormatter();
		  System.out.println("--------------------------------------------------------");
		  formater.printHelp("Kalkulator", options);
		  System.out.println("--------------------------------------------------------");
	}
	

}