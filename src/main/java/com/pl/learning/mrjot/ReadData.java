package com.pl.learning.mrjot;

import java.util.Scanner;

public class ReadData {
	
	private double firstNumber;
	
	private double secondNumber;
	
	Scanner scan = new Scanner(System.in);
	
	public ReadData() {
		
	}
	
	public double getFirstNumber() {
		return firstNumber;
	}

	public void setFirstNumber(double firstNumber) {
		this.firstNumber = firstNumber;
	}

	public double getSecondNumber() {
		return secondNumber;
	}

	public void setSecondNumber(double secondNumber) {
		this.secondNumber = secondNumber;
	}

	public void getDataFromUser() {
		try {
			System.out.println("Podaj pierwszą liczbę: ");
			this.firstNumber = Double.parseDouble(scan.nextLine());
			System.out.println("Podaj drugą liczbę: ");
			this.secondNumber = Double.parseDouble(scan.nextLine());
		}catch(IllegalArgumentException e) {
			System.out.println("Niewłaściwy format danych !");
			getDataFromUser();
		}
		
	}
	
	public void printResult(double a) {
		
		System.out.println("Twój wynik to: "+a);
		
	}
	

}
