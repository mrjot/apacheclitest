package com.pl.learning.mrjot;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import junit.framework.TestCase;

public class OperationTests extends TestCase {

	@Rule
	public final ExpectedException exception = ExpectedException.none();

	@Test
	public void testSumCalculations() {
		assertEquals(6, Operations.sum(2.5, 3.5), 0.0000001);
	}

	@Test
	public void testCorrectDivCalculations() {
		assertEquals(12, Operations.div(24, 2), 0.0000001);
	}

	@Test
	public void testWrongDivCalculations() {
		exception.expect(java.lang.ArithmeticException.class);
		exception.expectMessage("Dzielenie przez 0 !");
		Operations.div(12, 0);
	}

	@Test
	public void testMultCalculations() {
		assertEquals(6, Operations.mult(2, 3), 0.0000001);
	}

	@Test
	public void testSubCalculations() {
		assertEquals(6, Operations.sub(16, 10), 0.0000001);
	}

}
